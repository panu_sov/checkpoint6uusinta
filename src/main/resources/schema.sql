create table tilaus(
  id serial primary key,
  asiakasnimi varchar(255) not null,
  kommentit varchar(255),
  tilauksen_pvm date,
  prioriteetti integer
);
